import java.util.Scanner;  // Import the Scanner class

class PecahString {
    public static void main(String[ ] args) {
        Scanner myObj = new Scanner(System.in);  // Create a Scanner object
        System.out.println("Enter username");

        String userName = myObj.nextLine();  // Read user input
        System.out.println("Username is: " + userName);  // Output user input
        System.out.println("================================================");
        System.out.println("String Penuh: " + userName);
        System.out.println("================================================");
        System.out.println("proses memecah kata/kalimat menjadi karakter...");
        System.out.println("================================================");
        System.out.println("Done.");
        System.out.println("================================================");
  
        // Creating array of string length
        char[] huruf = new char[userName.length()];
  
        // Copy character by character into array
        for (int i = 0; i < userName.length(); i++) {
            huruf[i] = userName.charAt(i);
        }
        
        for (int i=0; i<huruf.length; i++) 
        { 
            char var = huruf[i];
            System.out.println(String.format("karakter ke %d adalah %c", i+1, var));
        }
        System.out.println("================================================");
  
    }
}
